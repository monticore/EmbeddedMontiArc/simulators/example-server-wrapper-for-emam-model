FROM java:8-jre as generator
ENV EMAM2CPP_URL https://github.com/EmbeddedMontiArc/EMAM2Cpp/releases/download/v0.0.9/emam2cpp.jar
RUN wget -O /emam2cpp.jar $EMAM2CPP_URL
COPY model /model
ENV ROOT_MODEL de.rwth.armin.modeling.autopilot.autopilot
# generate c++ from EMAM model
RUN rm -rf /cpp-gen \
  && mkdir /cpp-gen \
  && java -jar /emam2cpp.jar \
  --models-dir=/model \
  --root-model=$ROOT_MODEL \
  --output-dir=/cpp-gen \
  --flag-use-armadillo-backend \
  --flag-generate-server-wrapper



FROM ubuntu:18.04

ENV WORK_DIR /work
WORKDIR $WORK_DIR

COPY --from=generator /cpp-gen cpp-gen

RUN apt-get update && apt-get install -y \
  autoconf \
  build-essential \
  git \
  libarmadillo-dev \
  libspdlog-dev \
  libtool \
  lighttpd \
  pkg-config \
  && rm -rf /var/lib/apt/lists/*

# bug in generator: "armadillo.h" is emitted instead of <armadillo>
RUN cp /usr/include/armadillo /usr/include/armadillo.h

# install GRPC
ENV GRPC_VERSION v1.10.x
RUN git clone -b $GRPC_VERSION --depth=1 https://github.com/grpc/grpc \
  && cd grpc \
  && git submodule update --init \
  && make \
  && make install \
  && cd third_party/protobuf \
  && make install \
  && cd ../../.. \
  && rm -rf grpc

# build server
RUN cd cpp-gen && make

# publish proto file and start model server
RUN cp cpp-gen/model.proto /var/www/html/
EXPOSE 80/tcp
EXPOSE 10247
CMD /etc/init.d/lighttpd start && cpp-gen/server
